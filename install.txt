PyNVR is still in early stages and not really production ready.  But if you'd like to install it then here are the steps I used to install it on a fresh Ubuntu Linux install:

# Update packages we need and install them - python3, python3-pip, and git
sudo apt-get update
sudo apt-get install python3
sudo apt-get install python3-pip
sudo apt-get install git
sudo apt-get install ffmpeg

# Install bottle
pip3 install bottle


# Download pynvr
git clone https://gitlab.com/hoyle.hoyle/pynvr.git

# Change into the pynvr directory
cd pynvr

# Download submodules
git submodule init
git submodule update

# Change into the pylive555 submodule directory
cd pylive555

# Download live555
wget http://www.live555.com/liveMedia/public/live555-latest.tar.gz

# Uncompress
tar -xvzf live555-latest.tar.gz

# Change into the live555 directory
cd live

# Replace linux-64bit with your OS type
./genMakefiles linux-64bit

# To build live555
make

# Go back into pylive555
cd ..

# Edit the setup.py file
vi setup.py

# Change the line 8 from this:
                   library_dirs=['%s/%s' % (INSTALL_DIR, x) for x in ['liveMedia', 'UsageEnvironment', 'groupsock']],
# to this:
                   library_dirs=['%s/%s' % (INSTALL_DIR, x) for x in ['liveMedia', 'UsageEnvironment', 'BasicUsageEnvironment', 'groupsock']],


# Build pylive555
python3 setup.py build

# Change back into pynvr directory
cd ..

# Set the PYTHONPATH to point to the library you just built
# You can see this path in the output of building pylive555 above
export PYTHONPATH=pylive555/build/lib.linux-x86_64-3.4

# Take the nvr.json.template file and copy to nvr.json and edit
# If you don't know the URL to your camera try finding it here: https://www.soleratec.com/support/rtsp/rtsp_listing
cp nvr.json.template nvr.json
vi nvr.json

# WARNING WARNING - The reaper section of the config files will configure the system to automatically delete files when the disk starts to get full
# Keep this section empty if you don't want this to occur.  When initially testing, it might be good to keep this empty until it makes sense for you.

# Start the system, this will start recording of all enabled cameras along with starting the website on the defined interface/port
# If you edit the nvr.json file while the system is running, it will reload the config file dynamically (fingers crossed!)
./pynvr.py


# Note - The website is still being build, but you can see if at
#  http://localhost:8080/static/camera.html?camera_ids=camera1
#    Replace camera1 with the id you named your camera in nvr.json


